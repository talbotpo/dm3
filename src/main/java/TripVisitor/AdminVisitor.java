package TripVisitor;

import transportation.Trip;
import transportation.Station;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AdminVisitor implements Visitor {

    @Override
    public String visit(Trip trip) {
        List<String> results = new ArrayList<>();
        List<String> temp;
        List<Station> stops = new ArrayList<>();
        stops.add(trip.getOrigin());
        stops.add(trip.getDestination());
        //YUL-YYZ:
        temp = new ArrayList<>();
        stops.forEach(s -> temp.add(s.getCode()));
        results.add(String.join("-", temp));

        //[AIRCAN]AC481
        results.add("[" + trip.getTransportID()+ "]" + trip.getId());

        //(2014.11.28:06.00-2014.11.28:07:24)
        temp.clear();
        Date startDate = trip.getDepartureDatetime();

        results.add("(" + String.join("-", DateUtils.toString(
                DateUtils.addTimestampToDate(startDate, startDate.getTime())) + ")"));

        //|PS(0/12)474.00|AM(5/16)355.50|EL(150/200)237.00

        trip.getSections().forEach(section -> {
            BigDecimal price = section.getDiscountedPrice();
            results.add("|" + section.getType()
                    + "(" +section.getAvailableSpaces() +"/"
                    + section.getTotalPassengerPlaces()+")"
                    + price.toString());
        });

        return String.join("", results);
    }
}

