package gui;


import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import transportation.Station;

public class StationEditDialogController {

    @FXML
    private JFXTextField idField;

    @FXML
    private JFXTextField cityField;

    private Stage dialogStage;
    private Station station;
    private boolean okClicked = false;

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }


    public void setStation(Station station) {
        this.station = station;

        idField.setText(station.getCode());
        cityField.setText(station.getCity());
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */

    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */

    @FXML
    private void handleOk() {
        if (isInputValid()) {
            station.setCode(idField.getText());
            station.setCity(cityField.getText());

            okClicked = true;
            dialogStage.close();
        }
    }

    /*
     *
     * Called when the user clicks cancel.
     */


    @FXML
    private void handleCancel() {
        okClicked = false;
        dialogStage.close();
    }

    /*
     *
     * Validates the user input in the text fields.
     *
     * @return true if the input is valid
     */


    private boolean isInputValid() {
        String errorMessage = "";
        String newCode = idField.getText() == null ? null : idField.getText().trim();
        String newCity = cityField.getText() == null ? null : cityField.getText().trim();

        if (newCode == null || newCode.isEmpty() || !newCode.matches("[a-zA-Z]{3}")) {
            errorMessage += "The station code must be exactly 3 letters.\n";
        }
        if (newCity == null || newCity.isEmpty()) {
            errorMessage += "The city field is required.\n";
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields.");
            alert.setContentText(errorMessage);
            alert.showAndWait();
            return false;
        }
    }
}
