package reservation;

public interface IReservable {

    boolean reserve();

    boolean confirm();

    boolean cancelReservation();

}