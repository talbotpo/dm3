package money;

public interface IDiscountable {

    double getPricePercentage();

}