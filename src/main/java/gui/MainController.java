package gui;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

/**
 * File:
 * Description:
 * <p>
 * Package: gui
 * Project: dm3
 * <p>
 * Date: 2018-03-19
 *
 * @author Pierre-Olivier Talbot
 * @version 1.0
 * @since 1.9
 */
public class MainController {

    @FXML
    public void openAdmin() throws IOException  {
        Stage adminStage = new Stage();
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("admin.fxml")));
        adminStage.setTitle("AdminController");
        Scene s = new Scene(root);
        s.getStylesheets().add("main.css");
        adminStage.setScene(s);
        adminStage.show();
    }
}
