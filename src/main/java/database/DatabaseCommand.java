package database;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * <br/>
 * Created on 2018-03-27.
 *
 * @author Pierre-Olivier Talbot
 * @version 1.0
 */
public abstract class DatabaseCommand implements Command {

    protected ReservationSystemDatabase db = ReservationSystemDatabase.getInstance();
}
