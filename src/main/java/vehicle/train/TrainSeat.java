package vehicle.train;

import transportation.*;

public class TrainSeat extends Seat {
    @Override
    public boolean reserve() {
        return false;
    }

    @Override
    public boolean confirm() {
        return false;
    }

    @Override
    public boolean cancelReservation() {
        return false;
    }
}