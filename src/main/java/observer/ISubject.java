package observer;

/**
 * <br/>
 * Created on 2018-03-21.
 *
 * @author Pierre-Olivier Talbot
 * @version 1.0
 */
public interface ISubject {

    void attach(IObserver observer, int i);

    void detach(IObserver observer, int i);

    void notifyObservers(int i);
}
