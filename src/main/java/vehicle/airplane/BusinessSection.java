package vehicle.airplane;

public class BusinessSection extends AirplaneSection {
    @Override
    public double getPricePercentage() {
        return 0;
    }

    @Override
    public String getType() {
        return "B";
    }
}