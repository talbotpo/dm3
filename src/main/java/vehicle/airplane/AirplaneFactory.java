package vehicle.airplane;

import transportation.Business;
import transportation.Station;
import transportation.Transport;
import transportation.Trip;
import vehicle.VehicleFactory;

/**
 * <br/>
 * Created on 2018-03-20.
 *
 * @author Pierre-Olivier Talbot
 * @version 1.0
 */
public class AirplaneFactory extends VehicleFactory {
    private static AirplaneFactory instance;

    private AirplaneFactory() {
    }

    public static VehicleFactory getInstance() {
        return (instance == null) ? instance = new AirplaneFactory() : instance;
    }

    @Override
    public Station createStation() {
        return new Airport();
    }

    @Override
    public Business createBusiness(String id) {
        return new AirLine(id);
    }

    @Override
    public Transport createTransport(String id) {
        return new Airplane(id);
    }

    @Override
    public Trip createTrip() {
        return new Flight();
    }


}
