package transportation;

import money.PaymentInfo;
import reservation.*;

import java.util.Date;

public class TransportationPayableReservation extends PayableReservation {

    private PassengerSpace reservedSpace;

    public PassengerSpace getReservedSpace() {
        return this.reservedSpace;
    }

    /**
     * 
     * @param newSpace
     */
    public void setReservedSpace(PassengerSpace newSpace) {
        this.reservedSpace = newSpace;
    }

    /**
     * 
     * @param now
     */
    public Date getTimeUntilDeparture(Date now) throws UnsupportedOperationException{
        return now;
    }

    @Override
    public boolean pay(PaymentInfo info) {
        return false;
    }

    @Override
    public boolean refund() {
        return false;
    }
}