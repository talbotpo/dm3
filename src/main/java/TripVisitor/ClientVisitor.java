package TripVisitor;

import transportation.Section;
import transportation.Trip;
import transportation.Station;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClientVisitor implements Visitor {
    private String sectionType;

    // Note that you should use the class static type string for this
    // e.g. : new ClientTripVisitor(EconomicSection.type)
    public ClientVisitor(String sectionType){
        this.sectionType = sectionType;
    }

    // YUL-YYZ:[AIRCAN]AC481(2014.11.28:06.00-2014.11.28:07:24)|237.00|E50 (exemple pour les vols d'avion).
    // Cette donnée représente le vol AC481 par AIRCAN partant de YUL le 28/11/2014 à 6h arrivant à YYZ le
    // même jour à 7h24 ayant 50 sièges disponibles en classe Économie au prix de 237$.
    public String visit(Trip trip) throws IllegalArgumentException {
        List<String> results = new ArrayList<>();
        List<String> temp;
        List<Station> stops = new ArrayList<>();
        stops.add(trip.getOrigin());
        stops.add(trip.getDestination());
        //YUL-YYZ:
        temp = new ArrayList<>();
        stops.forEach(s -> temp.add(s.getCode()));
        results.add(String.join("-", temp));

        //[AIRCAN]AC481
        results.add("[" + trip.getTransportID()+ "]" + trip.getId());

        //(2014.11.28:06.00-2014.11.28:07:24)
        temp.clear();
        Date startDate = trip.getDepartureDatetime();

        results.add("(" + String.join("-", DateUtils.toString(
                DateUtils.addTimestampToDate(startDate, startDate.getTime())) + ")"));

        //|237.00|E50
        Section section = null;
        for (Section s : trip.getSections()){
            if(s.getType() == sectionType){
               section = s;
               break;
            }
        }

        if (section == null)
            throw new IllegalArgumentException("Section " + sectionType + " does not exist for this trip");
        BigDecimal price = section.getDiscountedPrice();
        int availablePlaces = section.getAvailableSpaces();
        results.add("|" + price + "|" + sectionType + availablePlaces);

        return String.join("", results);
    }
}
