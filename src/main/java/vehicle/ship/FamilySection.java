package vehicle.ship;

public class FamilySection extends CruiseShipSection {
    @Override
    public double getPricePercentage() {
        return 0;
    }

    @Override
    public String getType() {
        return "F";
    }
}