package transportationReservationSystem;

import observer.IObserver;
import transportation.Station;
import transportation.Trip;
import TripVisitor.Visitor;

import java.util.ArrayList;
import java.util.Date;

public abstract class Controller implements IObserver, Visitor {

    static ArrayList<Station> stations;

    protected Controller() {
        stations = new ArrayList<>();
    }

    /**
     * @param origin
     */
    public void findTripWithAvailableSpaces(Station origin) {
        // TODO - implement Controller.findTripWithAvailableSpaces
        throw new UnsupportedOperationException();
    }

    /**
     * @param origin
     * @param destination
     * @param departureDate
     */
    public void findTripWithAvailableSpaces(Station origin, Station destination, Date departureDate) {
        // TODO - implement Controller.findTripWithAvailableSpaces
        throw new UnsupportedOperationException();
    }

    /**
     * @param t1
     * @param t2
     */
    private ArrayList<Trip> getTripsIntersection(ArrayList<Trip> t1, ArrayList<Trip> t2) {
        // TODO - implement Controller.getTripsIntersection
        throw new UnsupportedOperationException();
    }

    public ArrayList<Station> getStations() {
        return stations;
    }


}