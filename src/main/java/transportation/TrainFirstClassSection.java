package transportation;

public class TrainFirstClassSection extends TrainSection {
    @Override
    public double getPricePercentage() {
        return 1;
    }

    @Override
    public String getType() {
        return "F";
    }
}
