package transportation;

public class AirplaneEconomySection extends AirplaneSection {
    @Override
    public double getPricePercentage() {
        return 0.5;
    }

    @Override
    public String getType() {
        return "E";
    }
}
