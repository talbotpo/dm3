package database;

import observer.IObserver;
import observer.ISubject;
import vehicle.airplane.AirplaneFactory;
import vehicle.airplane.Airport;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * <br/>
 * Created on 2018-03-21.
 *
 * @author Pierre-Olivier Talbot
 * @version 1.0
 */
public class ReservationSystemDatabase implements ISubject {

    public static final int AIRPORTS = 1;

    private static ReservationSystemDatabase instance;
    private static Connection db;
    private HashMap<Integer, ArrayList<IObserver>> observers;

    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    private ArrayList<Airport> airports;

    private ReservationSystemDatabase() {

        observers = new HashMap<>();
        observers.put(AIRPORTS, new ArrayList<>());

        try {
            connect();
            initialize();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void initialize() throws SQLException {
        airports = new ArrayList<>();
        String s = "SELECT * FROM airport";
        ResultSet rs;
        rs = db.prepareStatement(s).executeQuery();
        if (rs != null) {
            while (rs.next()) {
                Airport airport = (Airport) AirplaneFactory.getInstance().createStation();
                airport.setCode(rs.getString("code"));
                airport.setCity(rs.getString("city"));
                airports.add(airport);
            }
        }
    }

    public static ReservationSystemDatabase getInstance() {
        return instance == null ? instance = new ReservationSystemDatabase() : instance;
    }

    private void connect() throws SQLException {
        db = DriverManager.getConnection("jdbc:sqlite:res/main.db");
    }

    public void close() {
        if (db != null) {
            try {
                db.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void attach(IObserver observer, int i) {
        observers.get(i).add(observer);
    }

    @Override
    public void detach(IObserver observer, int i) {
        observers.get(i).remove(observer);
    }

    @Override
    public void notifyObservers(int i) {
        for (IObserver o : observers.get(i)) {
            o.update(i);
        }
    }

    private static void executeUpdate(PreparedStatement ps) {
        try {
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Airport> getAirports() {
        return airports;
    }

    public Connection getDb() {
        return db;
    }

    public ExecutorService getExecutor() {
        return executor;
    }

    public static class CreateAirportTask implements Runnable{

        private Airport airport;

        public CreateAirportTask(Airport airport) {
            this.airport = airport;
        }

        @Override
        public void run() {
            String s = "INSERT INTO airport(code,city) VALUES('" + airport.getCode() + "','" + airport.getCity() + "');";
            PreparedStatement ps;
            try {
                ps = db.prepareStatement(s);
                executeUpdate(ps);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    public static class DeleteAirportTask implements Runnable{

        private Airport toBeDeleted;

        public DeleteAirportTask(Airport toBeDeleted) {
            this.toBeDeleted = toBeDeleted;
        }

        @Override
        public void run() {
            String s = "DELETE FROM airport WHERE code='" + toBeDeleted.getCode() + "';";
            PreparedStatement ps;
            try {
                ps = db.prepareStatement(s);
                executeUpdate(ps);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
