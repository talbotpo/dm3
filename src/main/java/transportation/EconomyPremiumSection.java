package transportation;

public class EconomyPremiumSection extends AirplaneSection {
    @Override
    public double getPricePercentage() {
        return 0.6;
    }

    @Override
    public String getType() {
        return "EP";
    }
}
