package transportation;

import java.util.*;

public abstract class Station {

    private ArrayList<Trip> passingBy;
    private ArrayList<Trip> departures;
    private ArrayList<Trip> arrivals;
    private String code;
    private String city;

    public Station(){

    }
    public Station(String code,String city) {
        this.code = code;
        this.city = city;
    }

    public ArrayList<Trip> getTripsPassingBy() {
        return passingBy;
    }

    public ArrayList<Trip> getDepartures() {
        return departures;
    }

    public ArrayList<Trip> getArrivals() {
        return arrivals;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}