package TripVisitor;


public interface IVisitable {
    void accept(Visitor visitor);
}
