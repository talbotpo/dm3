package vehicle.airplane;

import transportation.*;

public class AirplaneSeat extends Seat {
    @Override
    public boolean reserve() {
        return false;
    }

    @Override
    public boolean confirm() {
        return false;
    }

    @Override
    public boolean cancelReservation() {
        return false;
    }
}