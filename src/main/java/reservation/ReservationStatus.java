package reservation;

public enum ReservationStatus {
    AVAILABLE,
    RESERVED,
    CONFIRMED
}