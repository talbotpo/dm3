package money;

import java.util.Date;

public class CreditCardPaymentInfo extends PaymentInfo {

    private String name;
    private long creditCardNumber;
    private Date expDate;
    private int secretCode;

}