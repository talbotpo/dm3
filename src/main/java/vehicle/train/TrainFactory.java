package vehicle.train;

import transportation.Business;
import transportation.Station;
import transportation.Transport;
import transportation.Trip;
import vehicle.VehicleFactory;

/**
 * <br/>
 * Created on 2018-03-21.
 *
 * @author Pierre-Olivier Talbot
 * @version 1.0
 */
public class TrainFactory extends VehicleFactory {

    private static TrainFactory instance;

    private TrainFactory() {
    }

    public static VehicleFactory getInstance() {
        return instance == null ? instance = new TrainFactory() : instance;
    }

    @Override
    public Station createStation() {
        return new TrainStation();
    }

    @Override
    public Business createBusiness(String id) {
        return new TrainLine(id);
    }

    @Override
    public Transport createTransport(String id) {
        return new Train(id);
    }

    @Override
    public Trip createTrip() {
        return new TrainItinerary();
    }
}
