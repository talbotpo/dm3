package gui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.stage.Modality;
import javafx.stage.Stage;
import database.ReservationSystemDatabase;

import java.util.Objects;
import java.util.Optional;

/**
 * File:
 * Description:
 * <p>
 * Package: gui
 * Project: dm3
 * <p>
 * Date: 2018-03-19
 *
 * @author Pierre-Olivier Talbot
 * @version 1.0
 * @since 1.9
 */
public class Main extends Application {

    @Override
    public void start(Stage mainStage) throws Exception {
        ReservationSystemDatabase.getInstance();

        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("main.fxml")));
        mainStage.setTitle("Reservation System");
        Scene s = new Scene(root);
        s.getStylesheets().add("main.css");
        mainStage.setResizable(false);
        mainStage.setOnCloseRequest(event -> {
            Alert closeConfirmation = new Alert(Alert.AlertType.CONFIRMATION, "Exiting will close all windows.");
            Button exitButton = (Button) closeConfirmation.getDialogPane().lookupButton(ButtonType.OK);
            exitButton.setText("Exit");
            closeConfirmation.setHeaderText("Are you sure you want to exit?");
            closeConfirmation.initModality(Modality.APPLICATION_MODAL);
            closeConfirmation.initOwner((Stage) event.getSource());
            Optional<ButtonType> closeResponse = closeConfirmation.showAndWait();
            if (closeResponse.isPresent() && ButtonType.OK.equals(closeResponse.get())) {
                Platform.exit();
            } else {
                event.consume();
            }
        });
        mainStage.setScene(s);
        mainStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
