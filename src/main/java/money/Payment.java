package money;

import java.math.BigDecimal;

public abstract class Payment {

    private BigDecimal total;

    public BigDecimal getTotal() {
        return this.total;
    }

    public String getReceipt() {
        // TODO - implement Payment.getReceipt
        throw new UnsupportedOperationException();
    }

    public boolean execute() {
        // TODO - implement Payment.execute
        throw new UnsupportedOperationException();
    }

    /**
     * 
     * @param total
     */
    public Payment(BigDecimal total) {
        // TODO - implement Payment.Payment
        throw new UnsupportedOperationException();
    }

}