package vehicle.airplane;

public class FirstClassSection extends AirplaneSection {
    @Override
    public double getPricePercentage() {
        return 0;
    }

    @Override
    public String getType() {
        return "FC";
    }
}