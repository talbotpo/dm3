package TripVisitor;

import transportation.Trip;

public interface Visitor {
    String visit(Trip trip);
}
