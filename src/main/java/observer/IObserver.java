package observer;

/**
 * <br/>
 * Created on 2018-03-21.
 *
 * @author Pierre-Olivier Talbot
 * @version 1.0
 */
public interface IObserver {
    void update(int i);
}
