package vehicle.airplane;

public class EconomySection extends AirplaneSection {
    @Override
    public double getPricePercentage() {
        return 0;
    }

    @Override
    public String getType() {
        return "E";
    }
}