package transportation;

public class BusinessSection extends AirplaneSection {
    @Override
    public double getPricePercentage() {
        return 0.75;
    }

    @Override
    public String getType() {
        return "B";
    }
}
