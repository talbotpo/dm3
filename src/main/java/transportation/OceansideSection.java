package transportation;

public class OceansideSection extends CabinSection {
    @Override
    public double getPricePercentage() {
        return 0.75;
    }

    @Override
    public String getType() {
        return "OS";
    }
}
