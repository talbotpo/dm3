package vehicle.train;

public class FirstClassSection extends TrainSection {
    @Override
    public double getPricePercentage() {
        return 0;
    }

    @Override
    public String getType() {
        return "FC";
    }
}