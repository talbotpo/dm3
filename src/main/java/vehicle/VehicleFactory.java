package vehicle;

import transportation.Business;
import transportation.Station;
import transportation.Transport;
import transportation.Trip;

/**
 * Abstract factory for vehicle-related objects.
 * <br/>
 * Created on 2018-03-20.
 *
 * @author Pierre-Olivier Talbot
 * @version 1.0
 */
public abstract class VehicleFactory {
    public abstract Station createStation();
    public abstract Business createBusiness(String id);
    public abstract Transport createTransport(String id);
    public abstract Trip createTrip();
}
