package vehicle.ship;

public class OceansideSection extends CruiseShipSection {
    @Override
    public double getPricePercentage() {
        return 0;
    }

    @Override
    public String getType() {
        return "OS";
    }
}