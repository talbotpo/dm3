package transportation;

public class TrainEconomySection extends TrainSection {
    @Override
    public double getPricePercentage() {
        return 0.5;
    }

    @Override
    public String getType() {
        return "E";
    }
}
