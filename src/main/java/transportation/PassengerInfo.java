package transportation;

import java.util.Date;

public class PassengerInfo {

    private String firstName;
    private String lastName;
    private String address;
    private String email;
    private String phone;
    private Date birthdate;
    private String passportNo;
    private Date passportExpiration;

}