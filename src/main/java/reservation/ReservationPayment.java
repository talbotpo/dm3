package reservation;

import money.*;

import java.math.BigDecimal;

public class ReservationPayment extends Payment {
    /**
     * @param total
     */
    public ReservationPayment(BigDecimal total) {
        super(total);
    }
}