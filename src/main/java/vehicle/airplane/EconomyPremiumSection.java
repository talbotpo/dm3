package vehicle.airplane;

public class EconomyPremiumSection extends AirplaneSection {
    @Override
    public double getPricePercentage() {
        return 0;
    }

    @Override
    public String getType() {
        return "EP";
    }
}