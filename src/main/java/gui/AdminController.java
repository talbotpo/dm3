package gui;


import com.jfoenix.controls.JFXButton;
import database.Command;
import database.CreateAirportCommand;
import database.DatabaseCommand;
import database.ReservationSystemDatabase;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Background;
import javafx.stage.Modality;
import javafx.stage.Stage;
import observer.IObserver;
import observer.ISubject;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import transportation.Station;
import vehicle.VehicleFactory;
import vehicle.airplane.AirplaneFactory;
import vehicle.airplane.Airport;

import java.io.IOException;
import java.util.Objects;


public class AdminController implements IObserver {

    private ReservationSystemDatabase db;

    private Command lastAirportCommand;

    public AdminController() {
    }

    @FXML
    private JFXButton undoAirportBtn;

    @FXML
    private TableView<Airport> airportsTable;

    @FXML
    private TableColumn<Airport, String> airportCodeColumn;

    @FXML
    private TableColumn<Airport, String> airportCityColumn;

    private static ObservableList<Airport> airports;

    @FXML
    public void initialize() {

        db = ReservationSystemDatabase.getInstance();
        db.attach(this, ReservationSystemDatabase.AIRPORTS);

        if (airports == null) {
            airports = FXCollections.observableArrayList();
            airports.addAll(db.getAirports());
        }

        airportCodeColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCode()));
        airportCityColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCity()));
        airportsTable.setItems(airports);
    }

    @Nullable
    private Station getNewStation(@NotNull VehicleFactory f) {
        Station newStation = f.createStation();
        boolean okClicked = showStationEditDialog(newStation);
        if (okClicked) {
            return newStation;
        }
        return null;
    }

    @FXML
    public void createAirport() {
        Station s = getNewStation(AirplaneFactory.getInstance());
        if (s != null) {
            CreateAirportCommand c = new CreateAirportCommand((Airport) s);
            c.execute();
            lastAirportCommand = c;
            undoAirportBtn.setDisable(false);
        }
    }

    @FXML
    public void undoAirport() {
        undoAirportBtn.setDisable(true);
        lastAirportCommand.undo();
        lastAirportCommand = null;
    }

    private boolean showStationEditDialog(Station station) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader(Objects.requireNonNull(getClass().getClassLoader().getResource("stationEditDialog.fxml")));
            Parent root = loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Add Station");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(airportsTable.getScene().getWindow());
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);

            // Set the person into the controller
            StationEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setStation(station);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void update(int i) {
        switch (i) {
            case ReservationSystemDatabase.AIRPORTS:
                airports.clear();
                airports.addAll(db.getAirports());
                break;
        }
    }
}
