package transportationReservationSystem;

import transportation.*;
import vehicle.VehicleFactory;

public class AdminController extends Controller {

    private static AdminController instance;
    private VehicleFactory factory;


    private AdminController() {
        super();
    }


    public static AdminController getInstance() {
        return instance == null ? instance = new AdminController() : instance;
    }

    /**
     * @param b
     */
    public void createBusiness(Business b) {
        // TODO - implement AdminController.createBusiness
        throw new UnsupportedOperationException();
    }

    /**
     * @param b
     */
    public boolean deleteBusiness(Business b) {
        // TODO - implement AdminController.deleteBusiness
        throw new UnsupportedOperationException();
    }

    public AdminController setVehicleFactory(VehicleFactory factory) {
        this.factory = factory;
        return this;
    }

    public void update(int i){

    }

    @Override
    public String visit(Trip trip) {
        return null;
    }
}