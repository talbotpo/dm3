package database;

import vehicle.airplane.Airport;

public class CreateAirportCommand extends DatabaseCommand {

    private Airport airport;

    public CreateAirportCommand(Airport airport) {
        this.airport = airport;
    }

    @Override
    public void execute() {
        db.getAirports().add(airport);
        db.getExecutor().submit(new ReservationSystemDatabase.CreateAirportTask(airport));
        db.notifyObservers(ReservationSystemDatabase.AIRPORTS);
    }

    @Override
    public void undo() {
        db.getAirports().remove(airport);
        db.getExecutor().submit(new ReservationSystemDatabase.DeleteAirportTask(airport));
        db.notifyObservers(ReservationSystemDatabase.AIRPORTS);
    }
}