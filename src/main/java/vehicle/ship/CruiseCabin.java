package vehicle.ship;

import transportation.*;

public class CruiseCabin extends Cabin {
    @Override
    public boolean reserve() {
        return false;
    }

    @Override
    public boolean confirm() {
        return false;
    }

    @Override
    public boolean cancelReservation() {
        return false;
    }
}