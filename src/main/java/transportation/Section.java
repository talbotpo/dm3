package transportation;

import money.*;

import java.math.BigDecimal;
import java.util.*;

public abstract class Section implements IDiscountable {

    private ArrayList<PassengerSpace> places;
    private int availableSpaces;

    public boolean hasAvailableSpaces() {
        return availableSpaces > 0;
    }

    public void incrementAvailableSpaces() {
        availableSpaces++;
    }

    public void decrementAvailableSpaces() {
        availableSpaces--;
    }

    public Trip getTrip() {
        // TODO - implement Section.getTrip
        throw new UnsupportedOperationException();
    }

    public BigDecimal getDiscountedPrice() {
        // TODO - implement Section.getDiscountedPrice
        throw new UnsupportedOperationException();
    }

    public int getAvailableSpaces(){
        return availableSpaces;
    }

    public int getTotalPassengerPlaces(){
        return places.size();
    }

    abstract public String getType();

}