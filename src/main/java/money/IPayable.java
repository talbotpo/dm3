package money;

public interface IPayable {

    /**
     * 
     * @param info
     */
    boolean pay(PaymentInfo info);

    boolean refund();

}