package transportationReservationSystem;

import java.util.*;
import transportation.*;

public class ClientController extends Controller {

    ArrayList<TransportationPayableReservation> reservations;
    private static ClientController instance;

    private ClientController() {
        super();
    }

    public static ClientController getInstance() {
        return instance == null ? instance = new ClientController() : instance;
    }
    /**
     * 
     * @param section
     * @param windowPriority
     */
    public void makeReservation(SeatSection section, boolean windowPriority) {
        // TODO - implement ClientController.makeReservation
        throw new UnsupportedOperationException();
    }

    /**
     * 
     * @param section
     */
    public void makeReservation(CabinSection section) {
        // TODO - implement ClientController.makeReservation
        throw new UnsupportedOperationException();
    }

    /**
     * 
     * @param reservationID
     * @param name
     * @param number
     * @param expirationDate
     * @param secretCode
     */
    public boolean payReservation(long reservationID, String name, long number, Date expirationDate, int secretCode) {
        // TODO - implement ClientController.payReservation
        throw new UnsupportedOperationException();
    }

    /**
     * 
     * @param reservationID
     */
    public void cancelReservation(long reservationID) {
        // TODO - implement ClientController.cancelReservation
        throw new UnsupportedOperationException();
    }

    /**
     * 
     * @param reservationID
     * @param section
     * @param windowPriority
     */
    public void modifyReservation(long reservationID, SeatSection section, boolean windowPriority) {
        // TODO - implement ClientController.modifyReservation
        throw new UnsupportedOperationException();
    }

    /**
     * 
     * @param reservationID
     * @param section
     */
    public void modifyReservation(long reservationID, CabinSection section) {
        // TODO - implement ClientController.modifyReservation
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(int i) {

    }

    @Override
    public String visit(Trip trip) {
        return null;
    }
}