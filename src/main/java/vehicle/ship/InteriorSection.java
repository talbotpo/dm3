package vehicle.ship;

public class InteriorSection extends CruiseShipSection {
    @Override
    public double getPricePercentage() {
        return 0;
    }

    @Override
    public String getType() {
        return "I";
    }
}