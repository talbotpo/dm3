package transportation;

public abstract class CabinSection extends Section {

    public int getCabinMaximumOccupancy() {
        // TODO - implement CabinSection.getCabinMaximumOccupancy
        throw new UnsupportedOperationException();
    }

    public Cabin reserveRandomCabin() {
        // TODO - implement CabinSection.reserveRandomCabin
        throw new UnsupportedOperationException();
    }

}