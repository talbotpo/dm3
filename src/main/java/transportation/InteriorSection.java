package transportation;

public class InteriorSection extends CabinSection{
    @Override
    public double getPricePercentage() {
        return 0.5;
    }

    @Override
    public String getType() {
        return "I";
    }
}
