package transportation;

import java.util.*;

public abstract class Business {

    ArrayList<Transport> transports;
    private String id;

    public Business(String id) {
        this.id = id;
    }

    /**
     * 
     * @param id
     */
    public void addTransport(String id) {
        // TODO - implement Business.addTransport
        throw new UnsupportedOperationException();
    }

    /**
     * 
     * @param transport
     */
    public void removeTransport(Transport transport) {
        // TODO - implement Business.removeTransport
        throw new UnsupportedOperationException();
    }

    public String getId() {
        return this.id;
    }

}