package transportation;

import TripVisitor.IVisitable;
import TripVisitor.Visitor;

import java.math.BigDecimal;
import java.util.*;

public abstract class Trip implements IVisitable {

    ArrayList<Station> stops;
    ArrayList<Section> sections;
    private String id;
    private Date departureDatetime;
    private Date arrivalDatetime;
    private BigDecimal price;
    private String transportID;

    public Station getOrigin() {
        // TODO - implement Trip.getOrigin
        throw new UnsupportedOperationException();
    }

    public Station getDestination() {
        // TODO - implement Trip.getDestination
        throw new UnsupportedOperationException();
    }

    public Date getDuration() {
        // TODO - implement Trip.getDuration
        throw new UnsupportedOperationException();
    }

    public ArrayList<Section> getSections() {
        return this.sections;
    }

    /**
     * 
     * @param id
     * @param departureDatetime
     * @param arrivalDatetime
     * @param price
     * @param stops
     */
    public void modify(String id, Date departureDatetime, Date arrivalDatetime, BigDecimal price, ArrayList<Station> stops) {
        // TODO - implement Trip.modify
        throw new UnsupportedOperationException();
    }

    public String getId() {
        return this.id;
    }

    public Date getDepartureDatetime() {
        return this.departureDatetime;
    }

    public Date getArrivalDatetime() {
        return this.arrivalDatetime;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public boolean hasAvailableSpaces() {
        // TODO - implement Trip.hasAvailableSpaces
        throw new UnsupportedOperationException();
    }

    public String getTransportID(){
        return this.transportID;
    }

    public void accept(Visitor visitor){
        visitor.visit(this);
    }

}