package vehicle.airplane;

import transportation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

public class Airplane extends Transport {

    public Airplane(String id) {
        super(id);
    }

    /**
     * @param id
     * @param departureDatetime
     * @param arrivalDatetime
     * @param price
     * @param sections
     * @param stops
     */
    public Flight createTrip(String id, Date departureDatetime, Date arrivalDatetime, BigDecimal price, ArrayList<AirplaneSection> sections, ArrayList<Airport> stops) {
        // TODO - implement Airplane.createTrip
        throw new UnsupportedOperationException();
    }

}