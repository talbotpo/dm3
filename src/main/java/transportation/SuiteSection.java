package transportation;

public class SuiteSection extends CabinSection {
    @Override
    public double getPricePercentage() {
        return 0.9;
    }

    @Override
    public String getType() {
        return "S";
    }
}
