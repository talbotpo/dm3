package transportation;

public class AirplaneFirstClassSection extends AirplaneSection {
    @Override
    public double getPricePercentage() {
        return 1;
    }

    @Override
    public String getType() {
        return "FC";
    }
}
