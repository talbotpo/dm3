package database;

import vehicle.airplane.Airport;

/**
 * <br/>
 * Created on 2018-03-27.
 *
 * @author Pierre-Olivier Talbot
 * @version 1.0
 */
public class DeleteAirportCommand extends DatabaseCommand {

    private Airport airport;

    public DeleteAirportCommand(Airport airport) {
        this.airport = airport;
    }

    @Override
    public void execute() {
        db.getAirports().remove(airport);
        db.getExecutor().submit(new ReservationSystemDatabase.DeleteAirportTask(airport));
        db.notifyObservers(ReservationSystemDatabase.AIRPORTS);
    }

    @Override
    public void undo() {
        db.getAirports().remove(airport);
        db.getExecutor().submit(new ReservationSystemDatabase.CreateAirportTask(airport));
        db.notifyObservers(ReservationSystemDatabase.AIRPORTS);
    }
}