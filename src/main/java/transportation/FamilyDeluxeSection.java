package transportation;

public class FamilyDeluxeSection extends CabinSection {
    @Override
    public double getPricePercentage() {
        return 1;
    }

    @Override
    public String getType() {
        return "FD";
    }
}
