package vehicle.ship;

public class SuiteSection extends CruiseShipSection {
    @Override
    public double getPricePercentage() {
        return 0;
    }

    @Override
    public String getType() {
        return "S";
    }
}