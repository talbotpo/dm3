package transportation;

import java.math.BigDecimal;
import java.util.*;

public abstract class Transport {

    Business owner;
    ArrayList<Trip> trips;
    private String id;
    ArrayList<Section> sections;

    public Transport(String id) {
        this.id = id;
    }

    /**
     * 
     * @param id
     */
    public void modify(String id) {
        this.id = id;
    }

    /**
     * 
     * @param id
     * @param departureDatetime
     * @param arrivalDatetime
     * @param price
     * @param stops
     */
    public Trip createTrip(String id, Date departureDatetime, Date arrivalDatetime, BigDecimal price, ArrayList<Station> stops) {
        // TODO - implement createTrip
        throw new UnsupportedOperationException();
    }

    /**
     * 
     * @param trip
     */
    public boolean deleteTrip(Trip trip) {
        // TODO - implement Transport.deleteTrip
        throw new UnsupportedOperationException();
    }

    public String getId(){
        return this.id;
    }

}