package vehicle.ship;

public class FamilyDeluxeSection extends CruiseShipSection {
    @Override
    public double getPricePercentage() {
        return 0;
    }

    @Override
    public String getType() {
        return "FD";
    }
}