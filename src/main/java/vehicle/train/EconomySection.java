package vehicle.train;

public class EconomySection extends TrainSection {
    @Override
    public double getPricePercentage() {
        return 0;
    }

    @Override
    public String getType() {
        return "E";
    }
}