package vehicle.ship;

import transportation.Business;
import transportation.Station;
import transportation.Transport;
import transportation.Trip;
import vehicle.VehicleFactory;

/**
 * <br/>
 * Created on 2018-03-21.
 *
 * @author Pierre-Olivier Talbot
 * @version 1.0
 */
public class ShipFactory extends VehicleFactory {

    private static ShipFactory instance;

    private ShipFactory() {
    }

    public static VehicleFactory getInstance() {
        return instance == null ? instance = new ShipFactory() : instance;
    }

    @Override
    public Station createStation() {
        return new Seaport();
    }

    @Override
    public Business createBusiness(String id) {
        return new CruiseLine(id);
    }

    @Override
    public Transport createTransport(String id) {
        return new Ship(id);
    }

    @Override
    public Trip createTrip() {
        return new CruiseItinerary();
    }
}
