package database;

/**
 * <br/>
 * Created on 2018-03-27.
 *
 * @author Pierre-Olivier Talbot
 * @version 1.0
 */
public interface Command {

    void execute();
    void undo();
}
